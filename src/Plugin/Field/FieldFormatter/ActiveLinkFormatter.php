<?php

namespace Drupal\active_link_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\link\Plugin\Field\FieldFormatter\LinkFormatter;

/**
 * Plugin implementation of the 'active_link' formatter.
 *
 * It adds the is-active link if the option is enabled. This option is not set
 * by default for Link fields.
 *
 * @FieldFormatter(
 *   id = "active_link",
 *   label = @Translation("Active Link"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class ActiveLinkFormatter extends LinkFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'include_active_class' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['include_active_class'] = [
      '#type' => 'checkbox',
      '#title' => t('Process the links to add the is-active class if needed'),
      '#default_value' => $this->getSetting('include_active_class'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $settings = $this->getSettings();

    if (!empty($settings['include_active_class'])) {
      $summary[] = t('Adds the is-active class if needed');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = parent::viewElements($items, $langcode);
    $settings = $this->getSettings();

    if (!empty($settings['include_active_class'])) {
      foreach ($element as &$item) {
        $item['#options'] += ['set_active_class' => TRUE];
      }
    }

    return $element;
  }

}
