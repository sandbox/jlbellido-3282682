<?php

namespace Drupal\Tests\active_link_formatter\FunctionalJavascript;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\link\LinkItemInterface;

/**
 * Verify handling of maintenance mode pages.
 *
 * @group metatag
 */
class ActiveLinkBrowserTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    // Modules for core functionality.
    'entity_test',
    'user',
    'field',
    'link',
    'active_link_formatter',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The field instance against we run the checks.
   *
   * @var \Drupal\field\FieldConfigInterface
   */
  protected $field;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $web_user = $this->drupalCreateUser([
      'view test entity',
    ]);
    if ($web_user) {
      $this->drupalLogin($web_user);
    }
  }

  /**
   * Put the site into maintenance mode, see what the meta tags are.
   */
  public function testUser1(): void {
    $this->setUpField();
    $field_name = $this->field->getName();

    // We create some entities of this type:
    $entity_test_1 = $this->container->get('entity_type.manager')
      ->getStorage('entity_test')
      ->create([
        'bundle' => 'entity_test',
        'name' => $this->randomMachineName(),
      ]);
    $entity_test_1->save();

    $link_text = $this->randomMachineName();
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity_test_2 */
    $entity_test_2 = $this->container->get('entity_type.manager')
      ->getStorage('entity_test')
      ->create([
        'bundle' => 'entity_test',
        'name' => $this->randomMachineName(),
        $field_name => [
          'uri' => 'internal:/entity_test/' . $entity_test_1->id(),
          'title' => $link_text,
        ],
      ]);
    $entity_test_2->save();

    // If the link points to any page it should not have the is-active class:
    $this->drupalGet('entity_test/' . $entity_test_2->id());
    $page = $this->getSession()->getPage();
    $link = $page->findLink($link_text);
    if ($link) {
      $this->assertFalse($link->hasClass('is-active'));
    }

    // If the link points to the same route where it is being rendered it should
    // have the 'is-active' class:
    $entity_test_2->get($field_name)->setValue([
      'uri' => 'internal:/entity_test/' . $entity_test_2->id(),
      'title' => $link_text,
    ]);
    $entity_test_2->save();

    $this->drupalGet('entity_test/' . $entity_test_2->id());
    $page = $this->getSession()->getPage();
    $link = $page->findLink($link_text);
    if ($link) {
      $this->assertTrue($link->hasClass('is-active'));
    }
  }

  /**
   * Setups the necessary field config to run the tests.
   */
  private function setUpField(): void {
    // Create and configure a field to run the checks:
    $field_name = mb_strtolower($this->randomMachineName());
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => $field_name,
      'entity_type' => 'entity_test',
      'type' => 'link',
    ]);
    $fieldStorage->save();

    $this->field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => 'entity_test',
      'settings' => [
        'title' => DRUPAL_DISABLED,
        'link_type' => LinkItemInterface::LINK_GENERIC,
      ],
    ]);
    $this->field->save();
    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
    $display_repository = \Drupal::service('entity_display.repository');
    $display_repository->getViewDisplay('entity_test', 'entity_test', 'full')
      ->setComponent($field_name, [
        'type' => 'active_link',
        'settings' => [
          'include_active_class' => TRUE,
        ],
      ])
      ->save();
  }

}
